import {Recyclability} from "./recyclability";

export class Item {
constructor(public id:string,

  public  itemName:string,

  public  material:string,
  public recyclability:Recyclability) {
}
}


