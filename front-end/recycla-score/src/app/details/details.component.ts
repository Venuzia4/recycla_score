import { Component, OnInit } from '@angular/core';
import {Item} from "../model/item";
import {ItemService} from "../service/item.service";
import {ActivatedRoute, ParamMap} from "@angular/router";

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {
item!:Item;
  constructor(private itemService:ItemService, private route: ActivatedRoute ) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe((params:ParamMap)=>{
      return this.itemService.getItem(<string>params.get('id')).subscribe((response)=>{
        console.log(response)
        this.item=response;
      })

    })
  }

}
