import {Component, OnInit} from '@angular/core';

import {ItemService} from "../service/item.service";
import {Router} from "@angular/router";
import {Item} from "../model/item";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  items: Item[] = [];
  item!: Item;
  selectedItem!: string;

  constructor(private itemService: ItemService, private router: Router) {
  }

  ngOnInit(): void {
    this.itemService.getItems().subscribe(item => {
      this.items = item;
    })
  }

  goToItemDetails(id: string) {
    this.router.navigate([`/details/${id}`]);
  }


}
