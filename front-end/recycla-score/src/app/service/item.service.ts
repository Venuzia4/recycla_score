import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Item} from "../model/item";
import {Observable} from "rxjs";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ItemService {
  items: Item[] = [];
  item!:Item;
  constructor(private http: HttpClient) { }
  readonly ITEMS = '/items'
  readonly ITEM = '/item'

  getItems(): Observable<Item[]> {
    return this.http.get<Item[]>(environment.API_URL + environment.ENDPOINT_ITEM + this.ITEMS);
  }

  getItem(id:string): Observable<Item> {
    return this.http.get<Item>(environment.API_URL + environment.ENDPOINT_ITEM + this.ITEM + '/'+id);

  }
}
