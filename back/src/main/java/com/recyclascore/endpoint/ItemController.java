package com.recyclascore.endpoint;

import com.recyclascore.model.entity.Item;
import com.recyclascore.service.ItemService;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class ItemController {

    private final ItemService itemService;

    public ItemController(ItemService itemService) {
        this.itemService = itemService;
    }


    @GetMapping(value ="/items")
    public List<Item> getItems(){
        return itemService.getAllItems();
    }


    @GetMapping(value = "/item/{id}")
    public Optional<Item> getItem(@PathVariable int id){
        return itemService.getItemById(id);
    }
}
