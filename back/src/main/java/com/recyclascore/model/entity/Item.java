package com.recyclascore.model.entity;

import com.recyclascore.enumeration.RecyclabityEnum;

import javax.persistence.*;

@Entity

public class Item {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "iditem", nullable = false)
    private int id;
    @Column(name = "name", nullable = false)
    private String itemName;
    @Column(name = "material", nullable = false)
    private String material;
    @Enumerated(EnumType.STRING)
    @Column(name = "recyclability", nullable = false)
    private RecyclabityEnum recyclability;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public RecyclabityEnum getRecyclability() {
        return recyclability;
    }

    public void setRecyclability(RecyclabityEnum recyclability) {
        this.recyclability = recyclability;
    }
}
