package com.recyclascore.service.impl;

import com.recyclascore.model.entity.Item;

import java.util.List;
import java.util.Optional;

public interface ItemServiceImpl   {
    List<Item> getAllItems();
    Optional<Item> getItemById(int id);

}
