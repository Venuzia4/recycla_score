package com.recyclascore.service;

import com.recyclascore.exception.ItemNotFoundException;
import com.recyclascore.model.entity.Item;
import com.recyclascore.model.repository.ItemRepository;
import com.recyclascore.service.impl.ItemServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
@Service

public class ItemService implements ItemServiceImpl {


    private final ItemRepository repository;

    public ItemService(ItemRepository repository) {this.repository = Objects.requireNonNull(repository,"repository is null");
    }

    @Override
    public List<Item> getAllItems() {
        return repository.findAll();
    }

    @Override
    public Optional<Item> getItemById(int id) {
        return Optional.ofNullable(repository.findById(id)
                .orElseThrow(() -> new ItemNotFoundException("Item not found with ID " + id)));
    }
}
