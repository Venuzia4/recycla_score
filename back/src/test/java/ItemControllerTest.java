import com.recyclascore.endpoint.ItemController;
import com.recyclascore.enumeration.RecyclabityEnum;
import com.recyclascore.model.entity.Item;
import com.recyclascore.model.repository.ItemRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;
@SpringBootTest(classes = ItemControllerTest.class)
@AutoConfigureMockMvc
@ComponentScan("com.recyclascore.endpoint")
@ComponentScan("com.recyclascore.service")
class ItemControllerTest {
    @MockBean
    private ItemRepository itemRepository;
    @Autowired
    private ItemController itemController;

    private List<Item>fakeItemList;

    @BeforeEach
    void setUp(){
        fakeItemList = new ArrayList<>();
        Item item1 = new Item();
        item1.setId(1);
        item1.setItemName("Fourchette en acier inoxydable");
        item1.setMaterial("Acier inoxydable");
        item1.setRecyclability(RecyclabityEnum.RECYCLABLE);
        fakeItemList.add(item1);

        Item item2 = new Item();
        item2.setId(2);
        item2.setItemName("Boîte en polystyrène expansé");
        item2.setMaterial("Polystyrène expansé");
        item2.setRecyclability(RecyclabityEnum.NON_RECYCLABLE);
        fakeItemList.add(item2);

    }

    @Test
    void getItems(){
        when(itemRepository.findAll()).thenReturn(fakeItemList);
        List<Item> result = itemController.getItems();
        assertEquals(fakeItemList, result);

    }


    @Test
    void testGetItem() {
        int itemId = 1;
        when(itemRepository.findById(itemId)).thenReturn(Optional.of(fakeItemList.get(0)));
        Optional<Item> result = itemController.getItem(itemId);
        assertTrue(result.isPresent());
        assertEquals(fakeItemList.get(0), result.get());
    }

}
